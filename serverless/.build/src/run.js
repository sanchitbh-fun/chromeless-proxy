"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
const chromeless_1 = require("chromeless");
const mqtt_1 = require("mqtt");
const utils_1 = require("./utils");
exports.default = ({ channelId, options }, context, callback, chromeInstance) => __awaiter(this, void 0, void 0, function* () {
    // used to block requests from being processed while we're exiting
    let endingInvocation = false;
    let timeout;
    let executionCheckInterval;
    utils_1.debug('Invoked with data: ', channelId, options);
    const chrome = new chromeless_1.LocalChrome(Object.assign({}, options, { remote: false, launchChrome: false, cdp: { closeTab: true } }));
    const queue = new chromeless_1.Queue(chrome);
    const TOPIC_CONNECTED = `chrome/${channelId}/connected`;
    const TOPIC_REQUEST = `chrome/${channelId}/request`;
    const TOPIC_RESPONSE = `chrome/${channelId}/response`;
    const TOPIC_END = `chrome/${channelId}/end`;
    const channel = mqtt_1.connect(utils_1.createPresignedURL());
    if (process.env.DEBUG) {
        channel.on('error', error => utils_1.debug('WebSocket error', error));
        channel.on('offline', () => utils_1.debug('WebSocket offline'));
    }
    /*
      Clean up function whenever we want to end the invocation.
      Importantly we publish a message that we're disconnecting, and then
      we kill the running Chrome instance.
    */
    const end = (topic_end_data = {}) => {
        if (!endingInvocation) {
            endingInvocation = true;
            clearInterval(executionCheckInterval);
            clearTimeout(timeout);
            channel.unsubscribe(TOPIC_END, () => {
                channel.publish(TOPIC_END, JSON.stringify(Object.assign({ channelId, chrome: true }, topic_end_data)), {
                    qos: 0,
                }, () => __awaiter(this, void 0, void 0, function* () {
                    channel.end();
                    yield chrome.close();
                    yield chromeInstance.kill();
                    callback();
                }));
            });
        }
    };
    const newTimeout = () => setTimeout(() => __awaiter(this, void 0, void 0, function* () {
        utils_1.debug('Timing out. No requests received for 30 seconds.');
        yield end({ inactivity: true });
    }), 30000);
    /*
      When we're almost out of time, we clean up.
      Importantly this makes sure that Chrome isn't running on the next invocation
      and publishes a message to the client letting it know we're disconnecting.
    */
    executionCheckInterval = setInterval(() => __awaiter(this, void 0, void 0, function* () {
        if (context.getRemainingTimeInMillis() < 5000) {
            utils_1.debug('Ran out of execution time.');
            yield end({ outOfTime: true });
        }
    }), 1000);
    channel.on('connect', () => {
        utils_1.debug('Connected to AWS IoT broker');
        /*
          Publish that we've connected. This lets the client know that
          it can start sending requests (commands) for us to process.
        */
        channel.publish(TOPIC_CONNECTED, JSON.stringify({}), { qos: 1 });
        /*
          The main bit. Listen for requests from the client, handle them
          and respond with the result.
        */
        channel.subscribe(TOPIC_REQUEST, () => {
            utils_1.debug(`Subscribed to ${TOPIC_REQUEST}`);
            timeout = newTimeout();
            channel.on('message', (topic, buffer) => __awaiter(this, void 0, void 0, function* () {
                if (TOPIC_REQUEST === topic && !endingInvocation) {
                    const message = buffer.toString();
                    clearTimeout(timeout);
                    utils_1.debug(`Message from ${TOPIC_REQUEST}`, message);
                    const command = JSON.parse(message);
                    try {
                        const result = yield queue.process(command);
                        const remoteResult = JSON.stringify({
                            value: result,
                        });
                        utils_1.debug('Chrome result', result);
                        channel.publish(TOPIC_RESPONSE, remoteResult, { qos: 1 });
                    }
                    catch (error) {
                        const remoteResult = JSON.stringify({
                            error: error.toString(),
                        });
                        utils_1.debug('Chrome error', error);
                        channel.publish(TOPIC_RESPONSE, remoteResult, { qos: 1 });
                    }
                    timeout = newTimeout();
                }
            }));
        });
        /*
          Handle diconnection from the client.
          Either the client purposfully ended the session, or the client
          connection was abruptly ended resulting in a last-will message
          being dispatched by the IoT MQTT broker.
          */
        channel.subscribe(TOPIC_END, () => __awaiter(this, void 0, void 0, function* () {
            channel.on('message', (topic, buffer) => __awaiter(this, void 0, void 0, function* () {
                if (TOPIC_END === topic) {
                    const message = buffer.toString();
                    const data = JSON.parse(message);
                    utils_1.debug(`Message from ${TOPIC_END}`, message);
                    utils_1.debug(`Client ${data.disconnected ? 'disconnected' : 'ended session'}.`);
                    yield end();
                    utils_1.debug('Ended successfully.');
                }
            }));
        }));
    });
});
//# sourceMappingURL=run.js.map